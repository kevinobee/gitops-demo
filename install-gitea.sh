#!/usr/bin/env bash

set -eo pipefail

echo "Install Gitea into cluster"
helm repo add gitea-charts https://dl.gitea.io/charts/
helm repo update
helm install -f gitea/values.yaml gitea gitea-charts/gitea --namespace gitea --create-namespace --atomic --wait --wait-for-jobs

echo "Configure access to Gitea"
kubectl --namespace default port-forward svc/gitea-http 3000:3000 > /dev/null 2>&1 &
HOST=127.0.0.1
PORT=3000
GITEA_HOST="${HOST}:${PORT}"
GITEA_URL="http://${GITEA_HOST}"

echo "Gitea available - ${GITEA_URL}"
